﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Collections.Generic;

namespace NetworkingExample
{
	class TcpServer {
		private bool listening;
		public bool IsListening {get { return listening; } }

		private TcpListener listener;
		private Thread listenerThread;
		private readonly List<Thread> workers;
		//сигналы остановки и принятого соединения
		private readonly ManualResetEvent stop, ready;

		public TcpServer() {
			listener = new TcpListener(
				new IPAddress(new byte[]{ 127, 0, 0, 1 }), 4000);
			workers = new List<Thread>();
			//создаём события
			ready = new ManualResetEvent(false);
			stop = new ManualResetEvent(false);
		}

		//Обработчик конкретного клиента
		private void TcpClientHandler(Object arg) {
			TcpClient client = (TcpClient) arg;
			NetworkStream stream = client.GetStream();
			byte[] buffer = new byte[1024];

			int nBytes = 0;
			while ((nBytes = stream.Read(buffer, 0, buffer.Length)) > 0) {
				string s = System.Text.Encoding.ASCII.GetString(buffer, 0, nBytes);
				Console.WriteLine("Received:{0}",s);
			}
			string OutStr = "Greeting!";
			byte[] msg = System.Text.Encoding.ASCII.GetBytes(OutStr);
			stream.Write(msg, 0, msg.Length);
			stream.Close();
			client.Close();
		}

		//Метод асинхронного запуска listener-а (в отдельном потоке)
		public void StartRunning() {
			listening = true;
			listenerThread = new Thread(new ThreadStart(HandleConnections));
			listenerThread.Start();
		}

		//остановка сервера актуальна при асинхронном запуске listener-а
		public void Stop() {
			listening = false;
			stop.Set();
			listenerThread.Join(); //дожидаемся завершения потока-слушателя (он завершится по IsRunning == false)
			foreach (Thread t in workers) {
				//дожидаемся завершения остальных потоков
				//t.Join(); // позволит потокам, уже принявшим клиента, дообработать его
				//Если хотим немедленного завершения. Но! Это весьма жёсткий способ завершения,
				//использовать его в реальных проектах не рекомендуется
				t.Abort();
			}
			listener.Stop();
		}

		private void HandleConnections() {
			listener.Start();
			//Бесконечный цикл ожидания подключений клиентов
			while (IsListening) {
				ready.Reset();
				Console.WriteLine("Waiting for clients");
				//принимаем клиентское подключение в одном потоке
				listener.BeginAcceptTcpClient(new AsyncCallback(AcceptCallback), listener);
				/* теперь мы ждём одного из сигналов синхронизации: stop или ready. 
				 WaitAny вернёт индекс события в массиве, которое произошло */
				if (WaitHandle.WaitAny(new[]{ stop, ready }) == 0) {
					//по stop выходим из цикла
					break;
				}
			}
			Console.WriteLine("ConnectionHandler stops");
		}

		//Обслуживаем поступившее клиентское соединение
		private void AcceptCallback(IAsyncResult res) {
			/* оповещаем слушающий Thread о том, что как минимум одно клиентское соединение к нам
		     * поступило и можно продолжать слушать дальше */
			ready.Set();
			//здесь мы получаем соединение от клиента
			if (IsListening) { // to prevent "Cannot access disposed object" when stop
				//заканчиваем принимать клиентское соединение и получаем объект TcpClient
				TcpClient client = listener.EndAcceptTcpClient(res);
				//Socket sock = listener.EndAcceptSocket(res);
				Thread t = new Thread(new ParameterizedThreadStart(TcpClientHandler));
				workers.Add(t);
				t.Start(client);
			}
		}
	}

	class MainClass {
		public static void Main(string[] args) {
			TcpServer server = new TcpServer();
			server.StartRunning();
			Console.WriteLine("Press ESC to stop server");
			while (server.IsListening) {
				//отсановим сервер по нажатию клавиши Esc
				ConsoleKeyInfo keyInfo = Console.ReadKey();
				if (keyInfo.Key == ConsoleKey.Escape) {
					Console.WriteLine("Exiting");
					server.Stop();
				}
			}
		}
	}
}
